'use strict';

const boton_registrar = document.querySelector('#btn_registrar');

const input_titulo = document.querySelector('#txt_titulo');
const input_precio = document.querySelector('#txt_precio');
const input_anioEd = document.querySelector('#txt_anioEd');
const input_edicion  = document.querySelector('#txt_Edicion');
const input_genero = document.getElementById("slt_genero");
const input_autor = document.getElementById("slt_autors");
const input_libroIsbn = document.querySelector('#txt_Isbn');
const input_descrip = document.querySelector('#txt_descrip');
const input_tipoLibro = document.getElementById("slt_libros");
const input_cantidad = document.querySelector('#txt_cantidad');
const input_frm = document.getElementById("frm_contacto");


function obtener_datos(){
    

    let titulo = input_titulo.value;
    let precio = input_precio.value;
    let anioEd = input_anioEd.value;
    let edicion = input_edicion.value;
    let genero = input_genero.value;
    let autor = input_autor.value;
    let libroIsbn = input_libroIsbn.value;
    let descrip = input_descrip.value;
    let tipoLibro = input_tipoLibro.value;
    let cantidad  = input_cantidad.value;
    let imgPortada = portadaSave;
    let imgContraPortada = portadaContraSave;
    
    
        if (!input_frm.checkValidity()) {
          
        } else {
            registrar_libro(titulo, precio ,anioEd, edicion,genero,autor ,libroIsbn,descrip,
                tipoLibro,cantidad,imgPortada,imgContraPortada );
                window.location.href = 'listar_libros.html';
        } 
        
        
};




boton_registrar.addEventListener("click", obtener_datos );