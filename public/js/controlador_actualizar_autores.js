'use strict';

// boton de actualizar en html de actualizar autor
const boton_actualizar = document.querySelector('#btn_actualizar');

// boton de eliminar en html de actualizar autor
const boton_eliminar = document.querySelector('#btn_eliminar');


const input_nombre = document.querySelector('#txt_nombre');
const input_biografia = document.querySelector('#txt_biografia');
const input_genero = document.querySelector('#txt_genero');
const input_libros = document.querySelector('#txt_libros');

let get_param = (param) => {
    var url_string =  window.location.href;
    var url = new URL(url_string);
    var id = url.searchParams.get(param);//Toma el parámetro id_autor del url y retorna el valor
    return id;
};

let _id = get_param('id_autor');

let autor = buscar_autor(_id);

//muestra la informacion del autor en el formulario de actualizacion
let mostrar_datos = () =>{
    input_nombre.value = autor[0]['nombre'];
    input_biografia.value = autor[0]['biografia'];
    input_genero.value = autor[0]['genero'];
    input_libros.value = autor[0]['libros'];
 
};

if(autor){
    mostrar_datos();
}
//actualiza la informacion del autor y refresca la pagina para devolverse a la lista
let obtener_datos = () =>{
    let nombre = input_nombre.value;
    let biografia = input_biografia.value;
    let genero = input_genero.value;
    let libros = input_libros.value;

    actualizar_autor(nombre, biografia, genero, libros, _id); 
    window.location.href = 'lista_autores.html';
    
};

//elimina autor de la base de datos, refresca la pagina para devolverse a la lista
let elimiar_autor = () => {    
    let _id = get_param('id_autor');
    borrar_autor(_id);    

    window.location.href = 'lista_autores.html';
}

//boton de actualizar que ejecuta la funcion
boton_actualizar.addEventListener('click', obtener_datos);

//boton de eliminar que ejecuta la funcion
boton_eliminar.addEventListener('click', elimiar_autor);

