'use strict';

function registrar_libro(ptitulo, pprecio ,panioEd, pedicion,pgenero,pautor ,plibroIsbn,pdescrip,
    ptipoLibro,pcantidad,pimgPortada,pimgContraPortada){
    let request = $.ajax({
        url : 'http://localhost:27017/api/registrar_libro',
        method : "POST",
        data : {
            titulo : ptitulo,
            precio :  pprecio,
            anioEd : panioEd,
            edicion :pedicion,
            genero : pgenero,
            autor : pautor,
            libroIsbn : plibroIsbn,
            descrip : pdescrip,
            tipoLibro : ptipoLibro,
            cantidad : pcantidad,
            imgPortada : pimgPortada,
            imgContraPortada : pimgContraPortada,
            califica : -1,
            rating1 : 0,
            rating2 : 0,
            rating3 : 0,
            rating4 : 0,
            rating5 : 0


        },
        dataType : "json",
        contentType : 'application/x-www-form-urlencoded; charset=UTF-8' 
    });

    request.done(function(res){
        swal.fire({
            type : 'success',
            title : 'Proceso realizado con éxito',
            text : res.msg
        });

    });

    request.fail(function(res){
        swal.fire({
            type : 'error',
            title : 'Proceso no realizado',
            text : res.msg
        });

    });
};

let listar_libros = () => {

    let libros = [];
  
    let request = $.ajax({
      url: "http://localhost:27017/api/listar_libro",
      method: "GET",
      data: {
      },
      dataType: "json",
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      async : false
    });
  
    request.done(function (res) {
        libros = res.lista_libros;
      
    });
  
    request.fail(function (jqXHR, textStatus) {
      
    });
    return libros;
   
  };

let buscar_libro = (id_libro) => {
    let libro = [];
  
    let request = $.ajax({
      url: "http://localhost:27017/api/buscar_libro/"+ id_libro,
      method: "GET",
      data: {
      },
      dataType: "json",
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      async : false
    });
  
    request.done(function (res) {
        libro = res.libro;
      
    });
  
    request.fail(function (jqXHR, textStatus) {
      
    });
    return libro;
   
  };

let actualizar_libro = (ptitulo, pprecio ,panioEd, pedicion,pgenero,pautor ,plibroIsbn,pdescrip,
    ptipoLibro,pcantidad,pimgPortada,pimgContraPortada,pcalifica,prating1,prating2,prating3,prating4,prating5,pid) =>{
    let request = $.ajax({
        url : 'http://localhost:27017/api/actualizar_libro',
        method : "POST",
        data : {
            titulo : ptitulo,
            precio :  pprecio,
            anioEd : panioEd,
            edicion :pedicion,
            genero : pgenero,
            autor : pautor,
            libroIsbn : plibroIsbn,
            descrip : pdescrip,
            tipoLibro : ptipoLibro,
            cantidad : pcantidad,
            imgPortada : pimgPortada,
            imgContraPortada : pimgContraPortada,
            califica : pcalifica,
            rating1 : prating1,
            rating2 : prating2,
            rating3 : prating3,
            rating4 : prating4,
            rating5 : prating5,
            id : pid
            
        },
        dataType : "json",
        contentType : 'application/x-www-form-urlencoded; charset=UTF-8' 
    });

    request.done(function(res){
        swal.fire({
            type : 'success',
            title : 'Proceso realizado con éxito',
            text : res.msg
        });

    });

    request.fail(function(res){
        swal.fire({
            type : 'error',
            title : 'Proceso no realizado',
            text : res.msg
        });

    });

};

let actualizar_libroCalif = (ptitulo, pprecio ,panioEd, pedicion,pgenero,pautor ,plibroIsbn,pdescrip,
    ptipoLibro,pcantidad,pimgPortada,pimgContraPortada,pcalifica,prating1,prating2,prating3,prating4,prating5,pid) =>{
    let request = $.ajax({
        url : 'http://localhost:27017/api/actualizar_libro',
        method : "POST",
        data : {
            titulo : ptitulo,
            precio :  pprecio,
            anioEd : panioEd,
            edicion :pedicion,
            genero : pgenero,
            autor : pautor,
            libroIsbn : plibroIsbn,
            descrip : pdescrip,
            tipoLibro : ptipoLibro,
            cantidad : pcantidad,
            imgPortada : pimgPortada,
            imgContraPortada : pimgContraPortada,
            califica : pcalifica,
            rating1 : prating1,
            rating2 : prating2,
            rating3 : prating3,
            rating4 : prating4,
            rating5 : prating5,
            id : pid
        },
        dataType : "json",
        contentType : 'application/x-www-form-urlencoded; charset=UTF-8' 
    });

    request.done(function(res){
        swal.fire({
            type : 'success',
            title : 'Proceso realizado con éxito',
            text : res.msg
        });

    });

    request.fail(function(res){
        swal.fire({
            type : 'error',
            title : 'Proceso no realizado',
            text : res.msg
        });

    });

};



let delete_libro = (id_libro) => {
  
  
    let request = $.ajax({
      url: "http://localhost:27017/api/eliminar_libro/"+ id_libro,
      method: "POST",
      data: {
      },
      dataType: "json",
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      async : false
    });
  
    request.done(function(res){
        swal.fire({
            type : 'success',
            title : 'Proceso realizado con éxito',
            text : res.msg
        });

    });

    request.fail(function(res){
        swal.fire({
            type : 'error',
            title : 'Proceso no realizado',
            text : res.msg
        });

    });
   
  };

  let buscar_libroAutorServi1 = (autor) => {
    
    let libro = [];
  
    let request = $.ajax({
      url: "http://localhost:27017/api/buscar_libroAutor/"+ autor,
      method: "GET",
      data: {
      },
      dataType: "json",
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      async : false
    });
  
    request.done(function (res) {
        libro = res.lista_libros;
      
    });
  
    request.fail(function (jqXHR, textStatus) {
      
    });
    return libro;
   
  };
  
  let buscar_libroByTitle = (titulo) => {
    
    let libro = [];
  
    let request = $.ajax({
      url: "http://localhost:27017/api/buscar_libroTitle/"+ titulo,
      method: "GET",
      data: {
      },
      dataType: "json",
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      async : false
    });
  
    request.done(function (res) {
        libro = res.lista_libros;
      
    });
  
    request.fail(function (jqXHR, textStatus) {
      
    });
    return libro;
   
  };

  let buscar_libroByISBN = (isbn) => {
    
    let libro = [];
  
    let request = $.ajax({
      url: "http://localhost:27017/api/buscar_libroISBN/"+ isbn,
      method: "GET",
      data: {
      },
      dataType: "json",
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      async : false
    });
  
    request.done(function (res) {
        libro = res.lista_libros;
      
    });
  
    request.fail(function (jqXHR, textStatus) {
      
    });
    return libro;
   
  };
  
  let buscar_libroByGen = (gen) => {
    
    let libro = [];
  
    let request = $.ajax({
      url: "http://localhost:27017/api/buscar_libroGen/"+ gen,
      method: "GET",
      data: {
      },
      dataType: "json",
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      async : false
    });
  
    request.done(function (res) {
        libro = res.lista_libros;
      
    });
  
    request.fail(function (jqXHR, textStatus) {
      
    });
    return libro;
   
  };
  
