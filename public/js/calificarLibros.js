'use strict';

const input_titulo = document.querySelector('#txt_titulo');

const input_imgPortada = document.getElementById('img_preview');
const btn_actualizar = document.querySelector('#btn_actualizar');
const input_frmAc = document.getElementById("frm_contactoAc");
var input_cali = -1;





let get_param = (param) => {
    var url_string =  window.location.href;
    var url = new URL(url_string);
    var id = url.searchParams.get(param);//Toma el parámetro id_inmueble del url y retorna el valor
    return id;
};


let _id = get_param('id_libros');

let libros = buscar_libro(_id);

let mostrar_datos =     () =>{
    input_titulo.value = libros[0]['titulo'];
    input_imgPortada.src = libros[0]['imgPortada'];
  
   
};

if(libros){
    mostrar_datos();
}

let obtener_datos = () =>{
    let titulo = libros[0]['titulo'];
    let precio = libros[0]['precio']; 
    let anioEd = libros[0]['anioEd'];
    let edicion = libros[0]['edicion'];
    let genero = libros[0]['genero'];
    let autor = libros[0]['autor'];
    let libroIsbn = libros[0]['libroIsbn'];
    let descrip = libros[0]['descrip'];
    let tipoLibro = libros[0]['tipoLibro'];
    let cantidad  = libros[0]['cantidad'];
    let imgPortada = libros[0]['imgPortada'];
    let imgContraPortada = libros[0]['imgContraPortada'];
       
    var rating1 = libros[0]['rating1'];
    var rating2 = libros[0]['rating2'];
    var rating3 = libros[0]['rating3'];
    var rating4 = libros[0]['rating4'];
    var rating5 = libros[0]['rating5'];

    var val = getRadioVal( document.getElementById('frm_contactoAc'), 'rbt' );
    input_cali = val;

    if(input_cali==1){
     rating1 = rating1 +1;
    }
   
   if(input_cali==2){
     rating2 = rating2 +1;
   }
   if(input_cali==3){
     rating3 = rating3 +1;
   }
   
   if(input_cali==4){
    rating4 = rating4 +1;
   }
   
   if(input_cali==5){
    rating5 = rating5 +1;
   }

   let  califica = calificaProm();  
 

    
    if (!input_frmAc.checkValidity()) {
          
    } else {
        actualizar_libro(titulo, precio ,anioEd, edicion,genero,autor ,libroIsbn,descrip,
            tipoLibro,cantidad,imgPortada,imgContraPortada,califica,rating1,rating2,rating3,rating4,rating5, _id); 
        window.location.href = 'Listar-Edit-Califica.html';
    } 
    
    function getRadioVal(form, name) {
        var val;
        // get list of radio buttons with specified name
        var radios = form.elements[name];
        
        // loop through list of radio buttons
        for (var i=0, len=radios.length; i<len; i++) {
            if ( radios[i].checked ) { // radio checked?
                val = radios[i].value; // if so, hold its value in val
                break; // and break out of for loop
            }
        }
        return val; // return value of checked radio or undefined if none checked
    };
    
    function redondeo2decimales(numero){
        var flotante = parseFloat(numero);
        var resultado = Math.round(flotante*100)/100;
        return resultado;
        };
    
    function calificaProm(){
    //(5*252 + 4*124 + 3*40 + 2*29 + 1*33) / (252+124+40+29+33)
    var calificaNow =(rating1*1 + 4*rating4+ 3*rating3+
    2*rating2+ 5*rating5) / (rating1 +rating2+
    rating3+rating4+rating5);
    
    var calificaGo = redondeo2decimales(calificaNow);
    
    return Math.trunc(calificaGo)
    };
};


btn_calificar.addEventListener('click', obtener_datos);