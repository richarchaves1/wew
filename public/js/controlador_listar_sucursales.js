'use strict';

let sucursales = listar_sucursales();
const tabla = document.querySelector('#tabla_sucursales');

let mostrar_sucursales = () =>{

    for(let i = 0; i < sucursales.length; i++){

        let fila = tabla.insertRow();
        
        fila.insertCell().innerHTML = sucursales[i]['nombre'];
        fila.insertCell().innerHTML = sucursales[i]['nombre_juridico'];
        fila.insertCell().innerHTML = sucursales[i]['codigo_sucursal'];
        fila.insertCell().innerHTML = sucursales[i]['email'];
        fila.insertCell().innerHTML = sucursales[i]['telefono'];
        fila.insertCell().innerHTML = sucursales[i]['provincia'];
        fila.insertCell().innerHTML = sucursales[i]['canton'];
        fila.insertCell().innerHTML = sucursales[i]['extra'];
      

        let celda_configuracion = fila.insertCell();

        // Creación del botón de editar
        let boton_editar = document.createElement('a');
        boton_editar.textContent = 'Editar';
        boton_editar.href = `actualizar_sucursales.html?id_sucursales=${sucursales[i]['_id']}`;
        celda_configuracion.appendChild(boton_editar);
        //Crear botton Delete
        let celda_configuracionEliminar = fila.insertCell();
        let boton_delete = document.createElement('a');
        boton_delete.textContent = 'Delete';
        boton_delete.href=(`borrar_sucursales.html?id_sucursales=${sucursales[i]['_id']}`)
        
        celda_configuracionEliminar.appendChild(boton_delete);
    }

};

mostrar_sucursales();


