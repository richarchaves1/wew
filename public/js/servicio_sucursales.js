'use strict';

function registrar_sucursales(pnombre, pnombre_juridico, pcodigo_sucursal, pemail, ptelefono,
    pprovincia , pcanton, pextra){
    let request = $.ajax({
        url : 'http://localhost:27017/api/registrar_sucursales',
        method : "POST",
        data : {
            
            nombre : pnombre,
            nombre_juridico :  pnombre_juridico,
            codigo_sucursal : pcodigo_sucursal,
            email : pemail,
            telefono : ptelefono,
            provincia : pprovincia,
            canton : pcanton,
            extra : pextra,
            estado : 'activo'
        },
        dataType : "json",
        contentType : 'application/x-www-form-urlencoded; charset=UTF-8' 
    });

    request.done(function(res){
        swal.fire({
            type : 'success',
            title : 'Proceso realizado con éxito',
            text : res.msg
        });

    });

    request.fail(function(res){
        swal.fire({
            type : 'error',
            title : 'Proceso no realizado',
            text : res.msg
        });

    });
};

let listar_sucursales = () => {

    let sucursales = [];
  
    let request = $.ajax({
      url: "http://localhost:27017/api/listar_sucursales",
      method: "GET",
      data: {
      },
      dataType: "json",
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      async : false
    });
  
    request.done(function (res) {
        sucursales = res.lista_sucursales;
      
    });
  
    request.fail(function (jqXHR, textStatus) {
      
    });
    return sucursales;
   
  };

let buscar_sucursales = (id_sucursales) => {
    let sucursales = [];
  
    let request = $.ajax({
      url: "http://localhost:27017/api/buscar_sucursales/"+ id_sucursales,
      method: "GET",
      data: {
      },
      dataType: "json",
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      async : false
    });
  
    request.done(function (res) {
        sucursales = res.sucursales;
      
    });
  
    request.fail(function (jqXHR, textStatus) {
      
    });
    return sucursales;
   
  };

let actualizar_sucursales = (pnombre, pnombre_juridico, pcodigo_sucursal, pemail, ptelefono,
    pprovincia , pcanton, pextra) =>{
    let request = $.ajax({
        url : 'http://localhost:27017/api/actualizar_sucursales',
        method : "POST",
        data : {
           
            nombre : pnombre,
            nombre_juridico :  pnombre_juridico,
            codigo_sucursal : pcodigo_sucursal,
            email : pemail,
            telefono : ptelefono,
            provincia : pprovincia,
            canton : pcanton,
            extra : pextra,
            estado : 'activo'
        },
        dataType : "json",
        contentType : 'application/x-www-form-urlencoded; charset=UTF-8' 
    });

    request.done(function(res){
        swal.fire({
            type : 'success',
            title : 'Proceso realizado con éxito',
            text : res.msg
        });

    });

    request.fail(function(res){
        swal.fire({
            type : 'error',
            title : 'Proceso no realizado',
            text : res.msg
        });

    });

};



let borrar_sucursales = (id_sucursales) => {
  
  
    let request = $.ajax({
      url: "http://localhost:27017/api/borrar_sucursales/"+ id_sucursales,
      method: "POST",
      data: {
      },
      dataType: "json",
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      async : false
    });
  
    request.done(function(res){
        swal.fire({
            type : 'success',
            title : 'Proceso realizado con éxito',
            text : res.msg
        });

    });

    request.fail(function(res){
        swal.fire({
            type : 'error',
            title : 'Proceso no realizado',
            text : res.msg
        });

    });
   
  };