'use strict';

let libros = listar_libros();
const tabla = document.querySelector('#tableTest tbody');
const sltFind = document.querySelector('#slt-findy')
const divSlave = document.getElementById('slt-SlaveDiv')
const lookBTN = document.querySelector('#Look');
var inputer = document.createElement('input');
var inputerISBN = document.createElement('input');

let mostrar_librosAu = (librosIn) =>{
        for(let i = 0; i < librosIn.length; i++){
            let fila = tabla.insertRow();
            let portada =librosIn[i]['imgPortada'];
            fila.insertCell().innerHTML = '<img src="' + portada + '""/>';
            
            fila.insertCell().innerHTML = librosIn[i]['titulo'];
            fila.insertCell().innerHTML = librosIn[i]['precio'];
            fila.insertCell().innerHTML = librosIn[i]['genero'];
            fila.insertCell().innerHTML = librosIn[i]['autor'];
            fila.insertCell().innerHTML = librosIn[i]['libroIsbn'];
            fila.insertCell().innerHTML = librosIn[i]['tipoLibro'];
            fila.insertCell().innerHTML = librosIn[i]['cantidad'];
             califica(fila, libros[i]['califica']);
    
        }
    
    };

let mostrar_libros = () =>{
    for(let i = 0; i < libros.length; i++){
        let fila = tabla.insertRow();
        let portada =libros[i]['imgPortada'];
        fila.insertCell().innerHTML = '<img src="' + portada + '""/>';
        
        fila.insertCell().innerHTML = libros[i]['titulo'];
        fila.insertCell().innerHTML = libros[i]['precio'];
        fila.insertCell().innerHTML = libros[i]['genero'];
        fila.insertCell().innerHTML = libros[i]['autor'];
        fila.insertCell().innerHTML = libros[i]['libroIsbn'];
        fila.insertCell().innerHTML = libros[i]['tipoLibro'];
        fila.insertCell().innerHTML = libros[i]['cantidad'];
         califica(fila, libros[i]['califica']);

    }

};

mostrar_libros();


function califica(fila,n){
   
  switch(n){
               case -1:
                        fila.insertCell().innerHTML = '<img src="images/Calificaimgs/scoreBlank.png' + '"/>'
                        +'<img src="images/Calificaimgs/scoreBlank.png' + '"/>'+'<img src="images/Calificaimgs/scoreBlank.png' + '"/>'+
                        '<img src="images/Calificaimgs/scoreBlank.png' + '"/>'+'<img src="images/Calificaimgs/scoreBlank.png' + '"/>'
                break;
                case 1:
                        fila.insertCell().innerHTML = '<img src="images/Calificaimgs/scorefill.png' + '"/>'
                        +'<img src="images/Calificaimgs/scoreBlank.png' + '"/>'+'<img src="images/Calificaimgs/scoreBlank.png' + '"/>'+
                        '<img src="images/Calificaimgs/scoreBlank.png' + '"/>'+'<img src="images/Calificaimgs/scoreBlank.png' + '"/>'
            
                break;
                case 2:
                        fila.insertCell().innerHTML = '<img src="images/Calificaimgs/scorefill.png' + '"/>'
                        +'<img src="images/Calificaimgs/scorefill.png' + '"/>'+'<img src="images/Calificaimgs/scoreBlank.png' + '"/>'+
                        '<img src="images/Calificaimgs/scoreBlank.png' + '"/>'+'<img src="images/Calificaimgs/scoreBlank.png' + '"/>'
            
                break;
                case 3:
                        fila.insertCell().innerHTML = '<img src="images/Calificaimgs/scorefill.png' + '"/>'
                        +'<img src="images/Calificaimgs/scorefill.png' + '"/>'+'<img src="images/Calificaimgs/scorefill.png' + '"/>'+
                        '<img src="images/Calificaimgs/scoreBlank.png' + '"/>'+'<img src="images/Calificaimgs/scoreBlank.png' + '"/>'
            
                break;
                case 4:
                        fila.insertCell().innerHTML = '<img src="images/Calificaimgs/scorefill.png' + '"/>'
                        +'<img src="images/Calificaimgs/scorefill.png' + '"/>'+'<img src="images/Calificaimgs/scorefill.png' + '"/>'+
                        '<img src="images/Calificaimgs/scorefill.png' + '"/>'+'<img src="images/Calificaimgs/scoreBlank.png' + '"/>'
            
                break;
                case 5:
                        fila.insertCell().innerHTML = '<img src="images/Calificaimgs/scorefill.png' + '"/>'
                        +'<img src="images/Calificaimgs/scorefill.png' + '"/>'+'<img src="images/Calificaimgs/scorefill.png' + '"/>'+
                        '<img src="images/Calificaimgs/scorefill.png' + '"/>'+'<img src="images/Calificaimgs/scorefill.png' + '"/>'
            
                break;

  }

    
     

    
}


function mostrarPerAutor(autor){
      let libros = buscar_libroAutorServi(autor);
       
       
        for(let i = 0; i < libros.length; i++){
                let fila = tabla.insertRow();
                let portada =libros[i]['imgPortada'];
                fila.insertCell().innerHTML = '<img src="' + portada + '""/>';
                
                fila.insertCell().innerHTML = libros[i]['titulo'];
                fila.insertCell().innerHTML = libros[i]['precio'];
                fila.insertCell().innerHTML = libros[i]['genero'];
                fila.insertCell().innerHTML = libros[i]['autor'];
                fila.insertCell().innerHTML = libros[i]['libroIsbn'];
                fila.insertCell().innerHTML = libros[i]['tipoLibro'];
                fila.insertCell().innerHTML = libros[i]['cantidad'];
                 califica(fila, libros[i]['califica']);
        
            }



};
function changesSltFind(){
 
 let value = sltFind.value;

 switch(value){

case "tituloTrue":

                deleteAll()  
inputer.type="text";
inputer.id="tituloIN";
inputer.required=true;
inputer.className="cupcakes";
divSlave.appendChild(inputer);
        
break;

case "autoresTrue":
                deleteAll()
                let genSelector2 = document.createElement("select");
                genSelector2.id="genSelector2";
               genSelector2.className="cupcakes";
                 divSlave.appendChild(genSelector2);
                
                 var array2 = ["Pedro","Juan","Maria"];
              
                 for (var i = 0; i < array2.length; i++) {
                   var option2 = document.createElement("option");
                   option2.value = array2[i];
                   option2.text = array2[i];
                   genSelector2.appendChild(option2);
               }
break;
case "isbnTrue":
                
                deleteAll()
                inputerISBN.type="text";
                inputerISBN.id="ISBNIN"
                inputerISBN.required=true;
                inputerISBN.className="cupcakes";
                inputerISBN.pattern="(?:(?=.{17}$)97[89][ -](?:[0-9]+[ -]){2}[0-9]+[ -][0-9]|97[89][0-9]{10}|(?=.{13}$)(?:[0-9]+[ -]){2}[0-9]+[ -][0-9Xx]|[0-9]{9}[0-9Xx])";
                inputerISBN.title="Debe ser ISBN valido de 13 digitos"      
                divSlave.appendChild(inputerISBN);
break;
case "generoTrue":
                deleteAll()
      let genSelector = document.createElement("select");
     genSelector.id="genSelector";
     genSelector.className="cupcakes";
     divSlave.appendChild(genSelector);
     
      var array = ["Comedia","Aventura","Romantica","Terror","Comedia Romantica","Infantil","Novela Adolecente"];
   
      for (var i = 0; i < array.length; i++) {
        var option = document.createElement("option");
        option.value = array[i];
        option.text = array[i];
        genSelector.appendChild(option);
    }


 break;
 default: 
  deleteAll()
 break;
 }


       function deleteAll(){
        let element = document.getElementById("slt-SlaveDiv");
        while (element.firstChild) {
          element.removeChild(element.firstChild);
        }
        let textDisp = document.createElement("label");
        textDisp.textContent="Llenar Info"

        divSlave.appendChild(textDisp);
       }
};

function BuscarBros(){
        let value = sltFind.value;

        switch(value){

                case "tituloTrue":
                                tabla.innerHTML = ""  ;            
                let inputSendTitle = document.getElementById("tituloIN");
                                let title=inputSendTitle.value;
                                let librosInTitle =[];
                            librosInTitle = buscar_libroByTitle(title); 
                            mostrar_librosAu(librosInTitle);
                      
                        
                break;
                
                case "autoresTrue":
                                tabla.innerHTML = ""  ;
                let inputSendAutores = document.getElementById("genSelector2");
                       let nameAutor=inputSendAutores.value;
                       let librosInAu =[];
                   librosInAu = buscar_libroAutorServi1(nameAutor);
                   mostrar_librosAu(librosInAu);
                break;
                case "isbnTrue":
                                tabla.innerHTML = ""  ;     
                                let inputSendIsbn = document.getElementById("ISBNIN");
                                let Isbn=inputSendIsbn.value;
                                let libroIsbn =[];
                                libroIsbn = buscar_libroByISBN(Isbn);
                            mostrar_librosAu(libroIsbn);
                              
                break;
                case "generoTrue":
                               
                                tabla.innerHTML = ""  ;
                                let inputSendGen = document.getElementById("genSelector");
                                       let nameGen=inputSendGen.value;
                                       let librosInGen =[];
                                   librosInGen = buscar_libroByGen(nameGen);
                                   mostrar_librosAu(librosInGen);
                
                 break;





        }



      

}

       
        
           
    
   

sltFind.addEventListener("change",changesSltFind );

lookBTN.addEventListener("click",BuscarBros);


