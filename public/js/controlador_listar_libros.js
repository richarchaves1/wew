'use strict';

let libros = listar_libros();
const tabla = document.querySelector('#tableTest tbody');



let mostrar_libros = () =>{
    for(let i = 0; i < libros.length; i++){
        let fila = tabla.insertRow();
        let portada =libros[i]['imgPortada'];
        fila.insertCell().innerHTML = '<img src="' + portada + '""/>';
        
        fila.insertCell().innerHTML = libros[i]['titulo'];
        fila.insertCell().innerHTML = libros[i]['precio'];
        fila.insertCell().innerHTML = libros[i]['genero'];
        fila.insertCell().innerHTML = libros[i]['autor'];
        fila.insertCell().innerHTML = libros[i]['libroIsbn'];
        fila.insertCell().innerHTML = libros[i]['tipoLibro'];
        fila.insertCell().innerHTML = libros[i]['cantidad'];
      

        let celda_configuracion = fila.insertCell();

        // Creación del botón de editar
        let boton_editar = document.createElement('a');
        boton_editar.textContent = 'Editar';
        boton_editar.href = `actualizarLibro.html?id_libros=${libros[i]['_id']}`;
        celda_configuracion.appendChild(boton_editar);
        //Crear botton Delete
        let celda_configuracionEliminar = fila.insertCell();
        let boton_delete = document.createElement('a');
        boton_delete.textContent = 'Delete';
        boton_delete.href=(`RedicDelete.html?id_libros=${libros[i]['_id']}`)
        
        celda_configuracionEliminar.appendChild(boton_delete);
    }

};

mostrar_libros();



    

