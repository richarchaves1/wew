'use strict';

let librerias = listar_librerias();
const tabla = document.querySelector('#tabla_librerias');

let mostrar_librerias = () =>{

    for(let i = 0; i < librerias.length; i++){

        let fila = tabla.insertRow();
        
        fila.insertCell().innerHTML = librerias[i]['nombre'];
        fila.insertCell().innerHTML = librerias[i]['nombre_juridico'];
        fila.insertCell().innerHTML = librerias[i]['codigo_sucursal'];
        fila.insertCell().innerHTML = librerias[i]['email'];
        fila.insertCell().innerHTML = librerias[i]['telefono'];
        fila.insertCell().innerHTML = librerias[i]['provincia'];
        fila.insertCell().innerHTML = librerias[i]['canton'];
        fila.insertCell().innerHTML = librerias[i]['extra'];
      

        let celda_configuracion = fila.insertCell();

        // Creación del botón de editar
        let boton_editar = document.createElement('a');
        boton_editar.textContent = 'Editar';
        boton_editar.href = `actualizar_librerias.html?id_librerias=${librerias[i]['_id']}`;
        celda_configuracion.appendChild(boton_editar);
        //Crear botton Delete
        let celda_configuracionEliminar = fila.insertCell();
        let boton_delete = document.createElement('a');
        boton_delete.textContent = 'Delete';
        boton_delete.href=(`borrar_libreria.html?id_librerias=${librerias[i]['_id']}`)
        
        celda_configuracionEliminar.appendChild(boton_delete);
    }

};

mostrar_librerias();


