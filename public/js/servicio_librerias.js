'use strict';

function registrar_libreria(pnombre, pnombre_juridico, pcodigo_sucursal, pemail, ptelefono,
    pprovincia , pcanton, pextra){
    let request = $.ajax({
        url : 'http://localhost:27017/api/registrar_libreria',
        method : "POST",
        data : {
            
            nombre : pnombre,
            nombre_juridico :  pnombre_juridico,
            codigo_sucursal : pcodigo_sucursal,
            email : pemail,
            telefono : ptelefono,
            provincia : pprovincia,
            canton : pcanton,
            extra : pextra,
            estado : 'activo'
        },
        dataType : "json",
        contentType : 'application/x-www-form-urlencoded; charset=UTF-8' 
    });

    request.done(function(res){
        swal.fire({
            type : 'success',
            title : 'Proceso realizado con éxito',
            text : res.msg
        });

    });

    request.fail(function(res){
        swal.fire({
            type : 'error',
            title : 'Proceso no realizado',
            text : res.msg
        });

    });
};

let listar_librerias = () => {

    let librerias = [];
  
    let request = $.ajax({
      url: "http://localhost:27017/api/listar_libreria",
      method: "GET",
      data: {
      },
      dataType: "json",
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      async : false
    });
  
    request.done(function (res) {
        librerias = res.lista_librerias;
      
    });
  
    request.fail(function (jqXHR, textStatus) {
      
    });
    return librerias;
   
  };

let buscar_libreria = (id_libreria) => {
    let libreria = [];
  
    let request = $.ajax({
      url: "http://localhost:27017/api/buscar_libreria/"+ id_libreria,
      method: "GET",
      data: {
      },
      dataType: "json",
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      async : false
    });
  
    request.done(function (res) {
        libreria = res.libreria;
      
    });
  
    request.fail(function (jqXHR, textStatus) {
      
    });
    return libreria;
   
  };

let actualizar_libreria = (pnombre, pnombre_juridico, pcodigo_sucursal, pemail, ptelefono,
    pprovincia , pcanton, pextra) =>{
    let request = $.ajax({
        url : 'http://localhost:27017/api/actualizar_libreria',
        method : "POST",
        data : {
           
            nombre : pnombre,
            nombre_juridico :  pnombre_juridico,
            codigo_sucursal : pcodigo_sucursal,
            email : pemail,
            telefono : ptelefono,
            provincia : pprovincia,
            canton : pcanton,
            extra : pextra,
            estado : 'activo'
        },
        dataType : "json",
        contentType : 'application/x-www-form-urlencoded; charset=UTF-8' 
    });

    request.done(function(res){
        swal.fire({
            type : 'success',
            title : 'Proceso realizado con éxito',
            text : res.msg
        });

    });

    request.fail(function(res){
        swal.fire({
            type : 'error',
            title : 'Proceso no realizado',
            text : res.msg
        });

    });

};



let borrar_libreria = (id_libreria) => {
  
  
    let request = $.ajax({
      url: "http://localhost:27017/api/borrar_libreria/"+ id_libreria,
      method: "POST",
      data: {
      },
      dataType: "json",
      contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
      async : false
    });
  
    request.done(function(res){
        swal.fire({
            type : 'success',
            title : 'Proceso realizado con éxito',
            text : res.msg
        });

    });

    request.fail(function(res){
        swal.fire({
            type : 'error',
            title : 'Proceso no realizado',
            text : res.msg
        });

    });
   
  };