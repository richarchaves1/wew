'use strict';
const sucursales_model = require ('./sucursales.model');

module.exports.registrar_sucursales = function(req, res){

  let nuevosucursales = new sucursales_model({
    
    nombre : req.body.nombre,
    nombre_juridico : req.body.nombre_juridico,
    codigo_sucursal : req.body.codigo_sucursal, 
    email : req.body.email,
    telefono : req.body.telefono,
    provincia : req.body.provincia, 
    canton : req.body.canton, 
    extra : req.body.extra, 
    estado : 'Activo'
    
    
   });
   
   
   nuevosucursales.save(function(error){
      if(error){

        res.json({
          success : false,
          msj: 'No se pudo registrar : '+ error

        });
     }else{
           res.json({
              success : true,
              msj: 'Se registro correctamente '


           });

      }


   });

};



module.exports.listar = function(req, res){
   sucursales_model.find().then(
       function(sucursales){
           if(sucursales.length > 0){
               res.json({success: true, lista_sucursales : sucursales});
           }else{
               res.json({success: false, lista_sucursales : sucursales});
           }
       }

   );
};

module.exports. buscar_por_id = function (req, res){
   
    sucursales_model.find({_id : req.body.id_sucursales}).then(
       function(sucursales){
           if(sucursales){
               res.json({success: true, sucursales : sucursales});
           }else{
               res.json({success: false, sucursales : sucursales});
           }
       }

   );

};

module.exports.actualizar = function(req, res){
  
   sucursales_model.findByIdAndUpdate(req.body.id, { $set: req.body },
       function (error){
           if(error){
               res.json({success : false , msg : 'No se pudo actualizar la sucursales'});
           }else{
               res.json({success: true , msg : 'La sucursales se actualizó con éxito'});
           }
       }
   
   );
};

module.exports.delete = function(req, res){
   
    sucursales_model.findByIdAndRemove(req.body.id_sucursales,
        function (error){
            if(error){
                res.json({success : false , msg : 'No se pudo actualizar el sucursales'});
            }else{
                res.json({success: true , msg : 'El sucursales se actualizó con éxito'});
            }
        }
    
    );

    
 };