'use strict';
const express = require ('express');
const router = express.Router();
const sucursalesApi = require ('./sucursales.api');

router.param('id_sucursales', function(req, res, next, id_sucursales){
  req.body.id_sucursales = id_sucursales;
  next();
});

router.route('/registrar_sucursales')
   .post(function(req,res){

         sucursalesApi.registrar_sucursales(req, res);

    });
    

    router.route('/actualizar_sucursales')
    .post(
        function(req , res){
          sucursalesApi.actualizar(req, res);
        }
    );


    router.route('/listar_sucursales')
    .get(
      function(req,res){
 
      sucursalesApi.listar(req, res);
 
     });    
 

router.route('/buscar_sucursales/:id_sucursales')
.get(
  function(req , res){
      sucursalesApi.buscar_por_id(req, res);
  }
);
router.route('/borrar_sucursales/:id_sucursales')
    .post(
        function(req , res){
          sucursalesApi.delete(req, res);
        }
    );

module.exports = router;