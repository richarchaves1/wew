'use strict';
const libreria_model = require ('./libreria.model');

module.exports.registrar_libreria = function(req, res){

  let nuevolibreria = new libreria_model({
    
    nombre : req.body.nombre,
    nombre_juridico : req.body.nombre_juridico,
    codigo_sucursal : req.body.codigo_sucursal, 
    email : req.body.email,
    telefono : req.body.telefono,
    provincia : req.body.provincia, 
    canton : req.body.canton, 
    extra : req.body.extra, 
    estado : 'Activo'
    
    
   });
   
   
   nuevolibreria.save(function(error){
      if(error){

        res.json({
          success : false,
          msj: 'No se pudo registrar : '+ error

        });
     }else{
           res.json({
              success : true,
              msj: 'Se registro correctamente '


           });

      }


   });

};



module.exports.listar = function(req, res){
   libreria_model.find().then(
       function(librerias){
           if(librerias.length > 0){
               res.json({success: true, lista_librerias : librerias});
           }else{
               res.json({success: false, lista_librerias : librerias});
           }
       }

   );
};

module.exports. buscar_por_id = function (req, res){
   
    libreria_model.find({_id : req.body.id_libreria}).then(
       function(libreria){
           if(libreria){
               res.json({success: true, libreria : libreria});
           }else{
               res.json({success: false, libreria : libreria});
           }
       }

   );

};

module.exports.actualizar = function(req, res){
  
   libreria_model.findByIdAndUpdate(req.body.id, { $set: req.body },
       function (error){
           if(error){
               res.json({success : false , msg : 'No se pudo actualizar la libreria'});
           }else{
               res.json({success: true , msg : 'La libreria se actualizó con éxito'});
           }
       }
   
   );
};

module.exports.delete = function(req, res){
   
    libreria_model.findByIdAndRemove(req.body.id_libreria,
        function (error){
            if(error){
                res.json({success : false , msg : 'No se pudo actualizar el libreria'});
            }else{
                res.json({success: true , msg : 'El libreria se actualizó con éxito'});
            }
        }
    
    );

    
 };