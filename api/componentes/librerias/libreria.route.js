'use strict';
const express = require ('express');
const router = express.Router();
const libreriasApi = require ('./libreria.api');

router.param('id_libreria', function(req, res, next, id_libreria){
  req.body.id_libreria = id_libreria;
  next();
});

router.route('/registrar_libreria')
   .post(function(req,res){

         libreriasApi.registrar_libreria(req, res);

    });
    

    router.route('/actualizar_libreria')
    .post(
        function(req , res){
          libreriasApi.actualizar(req, res);
        }
    );


    router.route('/listar_libreria')
    .get(
      function(req,res){
 
      libreriasApi.listar(req, res);
 
     });    
 

router.route('/buscar_libreria/:id_libreria')
.get(
  function(req , res){
      libreriasApi.buscar_por_id(req, res);
  }
);
router.route('/borrar_libreria/:id_libreria')
    .post(
        function(req , res){
          libreriasApi.delete(req, res);
        }
    );

module.exports = router;