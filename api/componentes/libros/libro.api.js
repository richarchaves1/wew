'use strict';
const libroModel = require ('./libro.model');

module.exports.registrar_libro = function(req, res){

  let nuevoLibro = new libroModel({
    titulo : req.body.titulo,
    precio : req.body.precio,
    anioEd : req.body.anioEd, 
    edicion : req.body.edicion, 
    genero : req.body.genero, 
    autor: req.body.autor, 
    libroIsbn : req.body.libroIsbn,
    descrip: req.body.descrip, 
    tipoLibro : req.body.tipoLibro, 
   cantidad  : req.body.cantidad, 
   imgPortada : req.body.imgPortada, 
   imgContraPortada : req.body.imgContraPortada, 
   estado : 'Activo',
    califica : req.body.califica,
    rating1 : req.body.rating1,
    rating2: req.body.rating2,
    rating3: req.body.rating3,
    rating4: req.body.rating4,
    rating5: req.body.rating5 
   });
   
   
   nuevoLibro.save(function(error){
      if(error){

        res.json({
          success : false,
          msj: 'El libro no pude ser registrado : '+ error

        });
     }else{
           res.json({
              success : true,
              msj: 'Se registro correctamente '


           });

      }


   });

};



module.exports.listar = function(req, res){
   libroModel.find().then(
       function(libros){
           if(libros.length > 0){
               res.json({success: true, lista_libros : libros});
           }else{
               res.json({success: false, lista_libros : libros});
           }
       }

   );
};

module.exports. buscar_por_id = function (req, res){
   
    libroModel.find({_id : req.body.id_libro}).then(
       function(libro){
           if(libro){
               res.json({success: true, libro : libro});
           }else{
               res.json({success: false, libro : libro});
           }
       }

   );

};

module.exports.actualizar = function(req, res){
  
   libroModel.findByIdAndUpdate(req.body.id, { $set: req.body },
       function (error){
           if(error){
               res.json({success : false , msg : 'No se pudo actualizar el libro'});
           }else{
               res.json({success: true , msg : 'El libro se actualizó con éxito'});
           }
       }
   
   );
};

module.exports.delete = function(req, res){
   
    libroModel.findByIdAndRemove(req.body.id_libro,
        function (error){
            if(error){
                res.json({success : false , msg : 'No se pudo actualizar el libro'});
            }else{
                res.json({success: true , msg : 'El libro se actualizó con éxito'});
            }
        }
    
    );

 };
 
 module.exports.FindByAutores = function(req, res){
      let autor = req.body.autor
       libroModel.find({autor : autor }).then(
        function(libros){
            if(libros.length > 0){
                res.json({success: true, lista_libros : libros});
            }else{
                res.json({success: false, lista_libros : libros});
            }
            
            
       }
 
    );

 };

 module.exports.FindByTitle = function(req, res){
    let title = req.body.titulo
     libroModel.find({titulo : title }).then(
      function(libros){
          if(libros.length > 0){
              res.json({success: true, lista_libros : libros});
          }else{
              res.json({success: false, lista_libros : libros});
          }
          
          
     }

  );

};

module.exports.FindByIsbn = function(req, res){
    let isbn = req.body.isbn
     libroModel.find({libroIsbn : isbn }).then(
      function(libros){
          if(libros.length > 0){
              res.json({success: true, lista_libros : libros});
          }else{
              res.json({success: false, lista_libros : libros});
          }
          
          
     }

  );

};
module.exports.FindByGen = function(req, res){
    let gen = req.body.gen
     libroModel.find({genero : gen }).then(
      function(libros){
          if(libros.length > 0){
              res.json({success: true, lista_libros : libros});
          }else{
              res.json({success: false, lista_libros : libros});
          }
          
          
     }

  );

};
